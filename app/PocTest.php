<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PocTest extends Model
{
    protected  $table = "poc_test";

    protected $primaryKey = "poc_id";
}
