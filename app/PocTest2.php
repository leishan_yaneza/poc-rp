<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PocTest2 extends Model
{
    protected  $table = "poc_test2";

    protected $primaryKey = "poc_id";
}
