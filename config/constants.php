<?php

return [
    'form_restrict_rules' => [
        'poc-test' => [
            'fields' => [
                'select_title' => [
                    'target_field' => 'bool_title',
                    'options' => [
                        'option1' =>  [
                            'text' => 'Option 1 Text',
                            'restrict' => ['on']
                        ],
                        'option2' =>  [
                            'text' => 'Option 2 Text',
                            'restrict' => []
                        ],
                    ]
                ],
                'radio_title' => [
                    'target_field' => 'description',
                    'options' => [
                        'radio1' =>  [
                            'text' => 'Radio Button 1 Text',
                            'restrict' => ['TEST5', 'TEST6']
                        ],
                        'radio2' =>  [
                            'text' => 'Radio Button 2 Text',
                            'restrict' => ['TEST7', 'TEST8']
                        ],
                    ]
                ]   
            ]
        ]
    ],
    'form_step_rules' => [
        'poc-test' => [
            'prereq' => [
                'model' => [
                    // 'PocTest2' => [
                    //     'message' =>'PocTest2 is required to be set first.',
                    //     'url' => '/admin/poc-test2'
                    // ]
                ]
            ],
            'next_step' => [
                'select_title' => [
                    'values' => ['option1'],
                    'message' => 'poc test 2',
                    'url' => '/admin/poc-test2'
                ],
                'radio_title' => [
                    'values' => ['radio1'],
                    'message' => 'poc test 3',
                    'url' => '/admin/poc-test3'
                ]
            ]
        ],
        'poc-test2' => [
            'prereq' => [
                'model' => [
                    'PocTest' => [
                        'message' =>'PocTest is required to be set first.',
                        'url' => '/admin/poc-test'
                    ]
                ]
            ],
            'next_step' => []
        ]
    ]
];
